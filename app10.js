const tabsTitle = document.querySelectorAll(".tabs-title")
const tabsItems = document.querySelectorAll(".tab-text")
tabsTitle.forEach(function(item) {
    item.addEventListener("click", function() { 
        let currentTitle = item
        let tabId = currentTitle.getAttribute("data-tab")
        let currentTab = document.querySelector(tabId)
        if (! currentTitle.classList.contains('active')) {
            tabsTitle.forEach(function(item) {
                item.classList.remove('active')
            });
            tabsItems.forEach(function(item) { 
                item.classList.remove('active')
            });
            currentTitle.classList.add('active')
            currentTab.classList.add('active')
        }
        
    });
});
